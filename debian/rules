#!/usr/bin/make -f
#export DH_VERBOSE=1
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

export BUILDDIR_STANDARD=obj-$(DEB_HOST_MULTIARCH)
export BUILDDIR_HIGHBITDEPTH=obj-$(DEB_HOST_MULTIARCH)-highbitdepth

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure --buildsystem=cmake --builddirectory="$(BUILDDIR_STANDARD)" -- \
		-DCMAKE_BUILD_TYPE:STRING=Release \
		-DCMAKE_INSTALL_PREFIX:PATH=/usr \
		-DCMAKE_LIBRARY_ARCHITECTURE="$(DEB_HOST_MULTIARCH)" \
		-DHIGH_BITDEPTH:BOOL='OFF'

	dh_auto_configure --buildsystem=cmake --builddirectory="$(BUILDDIR_HIGHBITDEPTH)" -- \
		-DCMAKE_BUILD_TYPE:STRING=Release \
		-DCMAKE_INSTALL_PREFIX:PATH=/usr \
		-DCMAKE_LIBRARY_ARCHITECTURE="$(DEB_HOST_MULTIARCH)" \
		-DHIGH_BITDEPTH:BOOL='ON'

override_dh_auto_build:
	# Build and install standard bit depth binaries regularly.
	dh_auto_build --buildsystem=cmake --builddirectory="$(BUILDDIR_STANDARD)"
	for filename in $$(find "bin" -maxdepth 1 -type f -executable); do \
		install -D -m755 "$$filename" "$(BUILDDIR_STANDARD)/usr/$$filename"; \
	done

	# Build high bit depth binaries and install with an updated name.
	dh_auto_build --buildsystem=cmake --builddirectory="$(BUILDDIR_HIGHBITDEPTH)"
	for filename in $$(find "bin" -maxdepth 1 -type f -executable | sed 's|bin/||'); do \
		install -D -m755 "bin/$$filename" "$(BUILDDIR_HIGHBITDEPTH)/usr/bin/$$(echo $$filename | sed 's/Static/StaticHighBitDepth/')"; \
	done

override_dh_compress:
	# Don't compress software manual and example configurations.
	dh_compress -X.pdf -X.cfg
